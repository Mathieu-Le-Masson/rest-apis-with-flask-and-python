import pygame

# Initialize Pygame
pygame.init()

# Set up the game window
window_size = (300, 300)
window = pygame.display.set_mode(window_size)
pygame.display.set_caption("Tic Tac Toe")

# Define colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)

# Define game variables
board = [['', '', ''],
         ['', '', ''],
         ['', '', '']]
player = 'X'
game_over = False

# Game loop
while not game_over:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_over = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # Get the coordinates of the mouse click
            pos = pygame.mouse.get_pos()
            col = pos[0] // 100
            row = pos[1] // 100

            # Make a move if the clicked position is valid
            if board[row][col] == '':
                board[row][col] = player
                if player == 'X':
                    player = 'O'
                else:
                    player = 'X'

    # Clear the window
    window.fill(WHITE)

    # Draw the board
    for row in range(3):
        for col in range(3):
            pygame.draw.rect(window, BLUE, (col * 100, row * 100, 100, 100), 3)
            if board[row][col] != '':
                font = pygame.font.Font(None, 100)
                text = font.render(board[row][col], True, BLACK)
                window.blit(text, (col * 100 + 30, row * 100 + 10))

    # Check for a winner
    for symbol in ['X', 'O']:
        for i in range(3):
            if board[i][0] == board[i][1] == board[i][2] == symbol:
                game_over = True
            if board[0][i] == board[1][i] == board[2][i] == symbol:
                game_over = True
        if board[0][0] == board[1][1] == board[2][2] == symbol:
            game_over = True
        if board[2][0] == board[1][1] == board[0][2] == symbol:
            game_over = True

    # Update the display
    pygame.display.flip()

# Quit Pygame
pygame.quit()

