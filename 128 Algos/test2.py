import numpy as np
import pandas as pd
import time

df = pd.DataFrame(np.random.randint(1, 100, size=(100000, 4)), columns=list('ABCD'))
df.shape
df.head()


def iterating():
    start_time = time.time_ns()
    for idx, row in df.iterrows():
        # creating a new column
        df.at[idx, 'ratio'] = 100 * (row["D"] / row["C"])
    print("Time taken by Iterative approach: ", time.time_ns() - start_time)


def vectorized():
    start_time = time.time_ns()
    df['ratio'] = 100 * (df['D'] / df['C'])
    print("Time taken by Vectorized approach: ", time.time_ns() - start_time)


if __name__ == '__main__':
    iterating()
    vectorized()
