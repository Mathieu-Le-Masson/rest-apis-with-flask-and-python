from db import db


class ItemModel(db.Model):
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)  # 80 characters max
    price = db.Column(db.Float(precision=2), nullable=False)  # 2 digits after the decimal point
    store_id = db.Column(db.Integer, db.ForeignKey('stores.id'), nullable=False)  # foreign key

    store = db.relationship('StoreModel', back_populates='items')  # backref: allows us to see which items are in a store
    tags = db.relationship('TagModel', back_populates='items', secondary="items_tags")
