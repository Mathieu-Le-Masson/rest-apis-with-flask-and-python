from db import db


class StoreModel(db.Model):
    __tablename__ = 'stores'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)  # 80 characters max

    # backref: allows us to see which items are in a store
    # lazy: don't load the items unless we ask for them
    # backref: allows us to see which items are in a store
    items = db.relationship('ItemModel', back_populates='store', lazy='dynamic', cascade="all, delete")
    tags = db.relationship('TagModel', back_populates='store', lazy='dynamic', cascade="all, delete")
