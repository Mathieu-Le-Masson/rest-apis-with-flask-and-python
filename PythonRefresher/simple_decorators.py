user = {"username": "jose", "access_level": "admin"}


def get_admin_password():
    return "1234"   # this is the admin password


def make_secure(func):
    def secure_function():  # this is the wrapper function, which will be returned by the make_secure function
        if user["access_level"] == "admin":
            return func()
        else:
            return f"No admin permissions for {user['username']}."
    return secure_function


get_admin_password = make_secure(get_admin_password)  # we are reassigning the get_admin_password variable to the secure_function function
                                                     # this is the same as: get_admin_password = secure_function, it is called decorating a function

print(get_admin_password())  # returns 1234

user.access_level = "guest"  # we are changing the user's access level to guest

print(get_admin_password())  # returns None, because the user's access level is not admin
