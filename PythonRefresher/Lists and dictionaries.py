import copy

numbers = [1, 3, 5]
doubled = [x * 2 for x in numbers]  # no need for a for loop with this syntax, this is called a list comprehension

friends = ["Rolf", "Sam", "Samantha", "Saurabh", "Jen"]
starts_s = [friend for friend in friends if friend.startswith("S")]  # list comprehension with if statement

for friend in friends:
    if friend.startswith("S"):   # this is the same as the list comprehension above, but with startswith() instead of the in operator
        starts_s.append(friend)
# Similar operators exist for strings, such as startswith() and endswith()

friends = ["Sam", "Samantha", "Saurabh"]
friends = friends + ["Jen"]  # this is the same as friends.append("Jen"), but it creates a new list instead of modifying the original list,
                                # it assigns the new list to the variable friends, so the old list is lost and will be garbage collected
new_friends = friends + ["Jen"]  # this creates a new list with the elements of friends and "Jen", but it doesn't modify friends
starts_s = [friend for friend in friends if friend.startswith("S")]  # list comprehension with if statement
print(friends is starts_s)  # False, because they are two different lists, even though they have the same elements
                            # this is because the list comprehension creates a new list, it doesn't modify the original list
print(id(friends), id(starts_s))  # the id() function returns the memory address of an object, we can see that the two lists have different memory addresses

friends2 = friends  # this doesn't create a new list, it just creates a new reference to the same list
copie = friends.copy()  # this creates a new list with the same elements as friends, but it doesn't create new copies of the elements of friends
                       # so if we modify the elements of friends, the elements of copy will also be modified because they are the same elements
deep_copy = copy.deepcopy(friends)  # this creates a new list with the same elements as friends, but it also creates new copies of the elements of friends
                                # this is useful if the elements of friends are mutable, such as lists or dictionaries
                                # because if we modify the elements of friends, the elements of deep_copy will not be modified
                                # but if the elements of friends are immutable, such as strings or numbers, then deep_copy is the same as copy

friend_ages = {"Rolf": 24, "Adam": 30, "Anne": 27}   # dictionary, key-value pairs, keys must be immutable (strings, numbers, tuples)
                                                     # and hashable (they have a hash() function) because they are used as the keys in a hash table
                                                     # A hash table is a data structure that uses a hash function to map keys to values
                                                     # A hash function is a function that takes an input and returns a number
                                                     # The hash function is used to determine where to store the key-value pair in memory
                                                     # The hash function is also used to determine where to look in memory to retrieve the key-value pair
                                                     # The hash function is also used to determine if two keys are the same or different
                                                     # If two keys are the same, then they will be stored in the same place in memory
print(friend_ages["Rolf"])   # this is how we access the value of a key in a dictionary
friend_ages["Bob"] = 20      # this is how we add a new key-value pair to a dictionary
friend_ages["Rolf"] = 25     # this is how we modify the value of a key in a dictionary
del friend_ages["Adam"]      # this is how we delete a key-value pair from a dictionary
print(friend_ages)           # this is how we print a dictionary

friends = [
    {"name": "Rolf", "age": 24},  # this is a list of dictionaries
    {"name": "Adam", "age": 30},
    {"name": "Anne", "age": 27}
]
print(friends[1]["name"])  # this is how we access the value of a key in a dictionary in a list
print(friend_ages.get("Rolf", 0))  # this is how we access the value of a key in a dictionary, but if the key doesn't exist, it returns the default value (0)

student_attendance = {"Rolf": 96, "Bob": 80, "Anne": 100}
for student in student_attendance:  # this is how we iterate over the keys in a dictionary
    print(f"{student} : {student_attendance[student]}%")    # this is how we access the value of a key in a dictionary

for student, attendance in student_attendance.items():  # this is how we iterate over the key-value pairs in a dictionary
    print(f"{student} : {attendance}%")    # this is how we access the value of a key in a dictionary, but we don't need to use the key to access the value

if "Bob" in student_attendance:  # this is how we check if a key exists in a dictionary
    print(f"Bob: attended {student_attendance['Bob']}% of his classes")  # this is how we access the value of a key in a dictionary
else:
    print("Bob is not a student in this class")

attendance_values = student_attendance.values()  # this is how we get a list of all the values in a dictionary
print(sum(attendance_values) / len(attendance_values))  # this is how we calculate the average of all the values in a dictionary

x = (1, 2, 3)  # this is a tuple, it is immutable, so we can't modify it
y = 1, 2, 3    # this is also a tuple, it is immutable, so we can't modify it
z = [(1, 2, 3), (4, 5, 6)]  # this is a list of tuples, here we need the parentheses to make it clear that we are creating tuples
a, b = 1, 2  # this is how we create two variables at the same time
a, b = b, a  # this is how we swap the values of two variables
d, e, f = x  # this is how we unpack a tuple into multiple variables

liste = list(student_attendance.items())  # this is how we convert a dictionary into a list of tuples, where each tuple is a key-value pair
                                            # the difference between .items() and .values() is that .items() returns a list of tuples, while .values() returns a list of values
                                            # the difference between .items() and .keys() is that .items() returns a list of tuples, while .keys() returns a list of keys
                                            # the difference between .items() and list(.items()) is that .items() returns a dict_items object, while list(.items()) returns a list
                                            # a dict_items object is a view object, which is a dynamic view of the dictionary's entries,
                                            # so if the dictionary changes, the dict_items object will also change
                                            # a dict_items object is not a list, so we can't use list methods on it, such as .append() or .pop()

for student, attendance in student_attendance.items():  # this is how we iterate over the key-value pairs in a dictionary
    print(f"{student} : {attendance}%")    # this is how we access the value of a key in a dictionary, but we don't need to use the key to access the value

for student, attendance in liste:  # this is how we iterate over the key-value pairs in a list of tuples
    print(f"{student} : {attendance}%")    # this is how we access the value of a key in a dictionary, but we don't need to use the key to access the value

# The difference here is that the first loop iterates over the key-value pairs in the dictionary, while the second loop iterates over the key-value pairs in the list of tuples

# The first loop is faster because it doesn't need to create a list of tuples, it just iterates over the key-value pairs in the dictionary, but if something changes the dictionary while we are iterating over it, then we will get an error

people = [("Bob", 42, "Mechanic"), ("James", 24, "Artist"), ("Harry", 32, "Lecturer")]

for name, age, profession in people:  # this is how we iterate over the tuples in a list of tuples
    print(f"Name: {name}, Age: {age}, Profession: {profession}")
    # if the tuples in the list of tuples have more or less than three elements, then we will get an error

person = ("Bob", 42, "Mechanic")
name, _, profession = person  # this is how we unpack a tuple into multiple variables, but we don't need to use all the variables

head, *tail = [1, 2, 3, 4, 5]  # this is how we unpack a list into multiple variables, but we don't need to use the variables but the first one
*head2, tail2 = [1, 2, 3, 4, 5]  # this is how we unpack a list into multiple variables, but we don't need to use the variables but the last one

print(head2)  # this is how we print a list
print(*head2)  # this is how we print a list, but we unpack it first so that we print the elements of the list instead of the list itself
