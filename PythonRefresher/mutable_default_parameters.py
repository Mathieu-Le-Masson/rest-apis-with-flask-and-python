from typing import List, Optional


class Student:
    def __init__(self, name: str, grades: List[int] = []):
        self.name = name
        self.grades = grades

    def take_exam(self, result: int):
        self.grades.append(result)


bob = Student("Bob")
rolf = Student("Rolf")
bob.take_exam(90)
print(bob.grades)  # [90]
print(rolf.grades)  # [90]  # this is not what we want, we want rolf.grades to be an empty list


# The problem is that the default value of the grades parameter is a mutable object, so it will be created once,
# and then it will be reused for all the instances of the Student class, so all the instances will have the same grades list,
# so when we change the grades list for one instance, it will be changed for all the instances.

# To fix this, we can do this:
class Student:      # this is the correct way to do it, we redefine the class, the old class will be garbage collected
    def __init__(self, name: str, grades: Optional[List[int]] = None):  # None
        self.name = name
        self.grades = grades or []  # this will create a new empty list for each instance of the Student class

    def take_exam(self, result: int):
        self.grades.append(result)


bob = Student("Bob")
rolf = Student("Rolf")
bob.take_exam(90)
print(bob.grades)  # [90]
print(rolf.grades)  # []  # this is what we want, rolf.grades is an empty list
