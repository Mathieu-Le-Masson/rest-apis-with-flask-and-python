from typing import Any


class Student:
    def __init__(self, name, grades):   # __init__ is a constructor
        self.name = name
        self.grades = grades

    def __setattr__(self, name: str, value: Any) -> None:   # __setattr__ is a setter
        super().__setattr__(name, value)

    def __getattribute__(self, name: str) -> Any:   # __getattribute__ is a getter
        return super().__getattribute__(name)

    def __str__(self) -> str:   # __str__ is a string representation of the object, it is used when we print the object,
                                # mostly for the user to see
        return f"Student {self.name}, {self.grades} grades"

    def __repr__(self) -> str:  # __repr__ is a representation of the object, it is used when we print the object,
                                # unless we have defined __str__ in the class, then __str__ is used instead
                                # it is mostly used for debugging
        return f"<Student ({self.name}, {self.grades})>"

    def average(self):
        """Returns the mean of the student's grades"""
        return sum(self.grades) / len(self.grades)


student = Student("Bob", (90, 90, 93, 78, 90))
student2 = Student("Rolf", (90, 90, 93, 78, 90))
student3 = Student("Jose", (32, 45, 67, 89, 12))

print(student.name, student.grades)
print(student.average())
print(student.__getattribute__("name"))
print(student.__getattribute__("grades"))

print(student)  # doesn't return <__main__.Student object at 0x0000020F4F6F4D30> because we have defined __str__ in the class,
                # so it returns Bob, (90, 90, 93, 78, 90)
print(repr(student))  # returns <Student (Bob, (90, 90, 93, 78, 90))> because we have defined __repr__ in the class