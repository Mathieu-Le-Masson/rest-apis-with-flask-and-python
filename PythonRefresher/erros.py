def divide(dividend, divisor):
    if divisor == 0:
        raise ZeroDivisionError("Divisor cannot be 0.")  # raise is used to raise an exception, this line will be executed if divisor is 0 and the program will stop
    return dividend / divisor


grades = []

print("Welcome to the average grade program.")
try:
    average = divide(sum(grades), len(grades))
except ZeroDivisionError as e:  # we can catch the exception and print a message
    # print(e) # this will print the message we passed to the exception
    print("There are no grades yet in your list.")
else:  # else is executed if the try block is executed without errors
    print(f"The average grade is {average}.")
finally:  # finally is always executed, regardless of errors
    print("Thank you!")

students = [
    {"name": "Bob", "grades": [75, 90]},
    {"name": "Rolf", "grades": []},
    {"name": "Jen", "grades": [100, 90]}
]

try:
    for student in students:
        name = student["name"]
        grades = student["grades"]
        average = divide(sum(grades), len(grades))
        print(f"{name} average: {average}")
except ZeroDivisionError:
    print(f"ERROR: {name} has no grades!")
else:
    print("-- All student averages calculated --")
finally:
    print("-- End of student average calculation --")
