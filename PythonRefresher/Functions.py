def user_age_in_seconds():
    user_age = 36
    age_seconds = user_age * 365 * 24 * 60 * 60
    return age_seconds


def print_seconds_age():
    print(
        f"Your age in seconds is {user_age_in_seconds()}")  # calls the function user_age_in_seconds() and prints the return value


def add(x, y):
    result = x + y
    print(result)


def say_hello(name, surname):
    print(f"Hello, {name} {surname}!")


def divide(dividend, divisor):
    if divisor != 0:
        print(dividend / divisor)
    else:
        print("You fool!")


def divide2(dividend, divisor):
    if divisor != 0:
        return dividend / divisor  # return exits the function
    else:
        return "You fool!"


# A bit of a contrived example, but it shows that you can return different types of values from the same function


def multiply(x,
             y=2):  # y is a default argument, x is a required argument, the default arguments must come after the required arguments
    return x * y


default_y = 3  # default_y is a global variable


def multiply2(x,
              y=default_y):  # this is not recommended because default_y is a global variable, and if default_y is changed, the function will not be affected,
    resultat = x * y  # because the function will use the value of default_y at the time the function was defined
    return resultat


def multiply3(x, y=None):  # this is recommended because None is immutable, so it will not be changed by the function
    if y is None:
        y = 2
    return x * y


def multiply4(x, y):
    return x * y
    print("This is never going to run")  # this is never going to run because return exits the function


def double(x):
    return x * 2


print_seconds_age()
add(5, 8)
say_hello("Bob", "Smith")
say_hello(surname="Smith", name="Bob")  # keyword arguments can be passed in any order

divide(15, 0)  # returns "You fool!"
divide(dividend=15,
       divisor=0)  # returns "You fool!", the python norm is not to put spaces around the = sign in keyword arguments
divide(15,
       divisor=0)  # returns "You fool!"  # keyword arguments can be used with positional arguments, but positional arguments must come first
# It is recommended to use keyword arguments for clarity

multiply(5)  # returns 10 because y is a default argument
# The difference between a parameter and an argument is that a parameter is a variable in a method definition.
# When a method is called, the arguments are the data you pass into the method's parameters.
# Parameter is variable in the declaration of function. Argument is the actual value of this variable that gets passed to function.

result = divide2(15, 3) * 5  # returns 25
result2 = divide2(15, 0) * 5  # returns "You fool!" * 5

multiple = lambda x, y: x * y  # this is a lambda function, it is an anonymous function, it is not recommended to use lambda functions in python
print(multiple(5, 8))  # returns 40
print((lambda x, y: x * y)(5, 8))  # returns 40, this is the same as the previous line, but it is not recommended to use lambda functions in python

sequence = [1, 3, 5, 9]
doubled = [double(x) for x in sequence]  # returns [2, 6, 10, 18] # this is a list comprehension, so it's a bit more efficient than the next line
doubled2 = map(double, sequence)  # returns [2, 6, 10, 18] # map is a built-in function that applies a function to each item in an iterable
doubled3 = list(map(double, sequence))  # returns [2, 6, 10, 18] # map returns a map object, so it must be converted to a list
doubled4 = [(lambda x: x * 2)(x) for x in sequence]  # returns [2, 6, 10, 18] # this is the same as the previous line, but it is not recommended to use lambda functions in python
doubled5 = list(map(lambda x: x * 2, sequence))  # returns [2, 6, 10, 18]