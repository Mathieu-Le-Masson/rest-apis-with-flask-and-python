from typing import List


def list_avg(sequence: List) -> float:
    return sum(sequence) / len(sequence)


# list_avg(123)  will not work because 123 is not a list
list_avg([123, 456])  # this will work because [123, 456] is a list


class Book:
    TYPES = ("hardcover", "paperback")  # this is a class attribute, it is shared by all instances of the class

    def __init__(self, name: str, book_type: str, weight: int) -> None:  # We type hint the parameters and the return value
        self.name = name
        self.book_type = book_type
        self.weight = weight

    def __repr__(self) -> str:  # __repr__ is a representation of the object, it is used when we print the object
        return f"<Book {self.name}, {self.book_type}, weighing {self.weight}g>"

    @classmethod
    def hardcover(cls, name: str, page_weight: int) -> "Book":  # We have to use "Book" as a string because the Book class is not defined yet
        return cls(name, cls.TYPES[0], page_weight + 100)

    @classmethod
    def paperback(cls, name: str, page_weight: int) -> "Book":  # We have to use "Book" as a string because the Book class is not defined yet
        return cls(name, cls.TYPES[1], page_weight)


class Bookshelf:
    def __init__(self, books: List[Book]) -> None:  # *books is a tuple of books
        self.books = books

    def __str__(self) -> str:
        return f"Bookshelf with {len(self.books)} books."
