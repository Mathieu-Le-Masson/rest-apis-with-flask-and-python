def named(**kwargs):    # **kwargs is a dictionary, it is a dictionary of all the keyword arguments that are passed in
    print(kwargs)       # when used as a parameter, **kwargs is used to pack keyword arguments into a dictionary,
                        # when used as an argument, **kwargs is used to unpack a dictionary into keyword arguments


named(name="Bob", age=25)   # returns {"name": "Bob", "age": 25}


details = {"name": "Bob", "age": 25}

named(**details)    # **details is the same as name="Bob", age=25 (the ** unpacks the dictionary)
                    # We still need the keys to match the parameter names

# named(**"Bob)    returns TypeError: 'str' object is not a mapping


def print_nicely(**kwargs):     # we pack the keyword arguments into a dictionary
    named(**kwargs)     # we unpack the dictionary into the named() function, which itself packs the keyword arguments into a dictionary
    for arg, value in kwargs.items():
        print(f"{arg}: {value}")


print_nicely(name="Bob", age=25)


def both(*args, **kwargs):  # *args is a tuple, **kwargs is a dictionary, we can use both at the same time but *args must come first
    print(args)
    print(kwargs)


both(1, 3, 5, name="Bob", age=25)  # returns (1, 3, 5) {"name": "Bob", "age": 25}