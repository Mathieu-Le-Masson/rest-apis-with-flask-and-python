def main():
    nombre = 2
    double = nombre * 2.365659
    print(f"Your number doubled is {double:.4}")  # f is for format
    double = nombre * 2.365659 + 2
    print(f"A percentage is {double / 10:.15%}")  # % is for percentage
    print(f"A scientific notation is {double / 10:.15e}")  # e is for scientific notation
    print(f"Your number squared is pow({nombre}, 2) = {pow(nombre, 2)}")  # pow is for power
    print(f"Your number squared is {nombre ** 2}")  # ** is for power
    print(f"Your number at the power of 4 is {pow(nombre, 4)}")
    # age = int(input("How old are you? "))
    # print(f"You are {age * 12 * 30 * 24} hours old")

    list = ["Bob", "Rolf", "Anne"]
    tuple = ("Bob", "Rolf", "Anne")  # immutable
    set = {"Bob", "Rolf", "Anne"}  # unique & unordered
    print(list[0], tuple[1], set)
    # tuple[0] = "Smith"   # TypeError: 'tuple' object does not support item assignment
    set.add("Smith")
    list.append("Smith")
    list.remove("Bob")
    print(list, tuple, set)
    print(list[0:2], tuple[1:], set)
    print(list[-1], tuple[-1], set.pop())  # set.pop() returns an arbitrary set element
    list.pop(-1)  # removes the last element

    friends = {"Bob", "Rolf", "Anne"}
    abroad = {"Bob", "Anne"}
    local = {"Rolf"}
    local_friends = friends.difference(abroad)  # can also be written as friends - abroad
    local_friends = friends - abroad
    print(local_friends)
    test = abroad.difference(friends)  # abroad - friends = {} because abroad is a subset of friends give set()
    print(test)
    allfriends = abroad.union(local)  # can also be written as abroad | local
    allfriends = abroad | local
    print(allfriends)

    art = {"Bob", "Jen", "Rolf", "Charlie"}
    science = {"Bob", "Jen", "Adam", "Anne"}
    both = art.intersection(science)  # can also be written as art & science
    both = art & science
    print(both)

    literature = {"Bob", "Adam", "Mary", "Martin"}
    print(art & science | literature)  # order of operations is important

    one = art.symmetric_difference(science)  # can also be written as art ^ science
    one = art ^ science  # returns elements that are in one set or the other but not both
    print(one)
    print("Bob" in art)  # returns True

    # Comparison operators: ==, !=, >, <, >=, <= (returns True or False) can be chained together with and/or
    # Logical operators: and, or, not are evaluated in this order: not, and, or
    # print(5 == 5)  # returns True
    # print(5 > 6)  # returns False
    # print(5 != 6)  # returns True
    # print(5 <= 6 and 5 != 4)  # returns True
    # print(5 >= 6 or 5 != 4)  # returns True
    # print(5 >= 6 or 5 != 4 and 5 == 5)  # returns False because and is evaluated before or
    # print(not 5 == 5)  # returns False
    # print(not 5 == 5 and 5 == 5)  # returns False because not is evaluated before and
    # print(not 5 == 5 or 5 == 5)  # returns True because not is evaluated before or

    print(friends == abroad)  # returns False because friends and abroad are not the same set
    print(friends == local_friends)  # returns True because friends and local_friends are the same set
    print(friends is local_friends)  # returns False because friends and local_friends are not the same object
    print(friends is friends)  # returns True because friends and friends are the same object


if __name__ == '__main__':  # this is the entry point of the program and is only executed if the file is run
                            # directly, in opposition to being imported
    main()
