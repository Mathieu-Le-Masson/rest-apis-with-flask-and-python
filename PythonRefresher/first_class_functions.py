from operator import itemgetter   # this is a module that contains functions corresponding to the operators of Python


def divide(dividend, divisor):
    if divisor == 0:
        raise ZeroDivisionError("Divisor cannot be 0.")
    return dividend / divisor


def calculate(*values, operator):
    return operator(*values)


result = calculate(20, 4,
                   operator=divide)  # we can pass a function as an argument, since functions are callable objects
print(result)


def search(sequence, expected, finder):  # we can pass a function as an argument, since functions are callable objects
    for elem in sequence:
        if finder(elem) == expected:
            return elem
    raise RuntimeError(f"Could not find an element with {expected}.")


friends = [
    {"name": "Rolf Smith", "age": 24},
    {"name": "Adam Wool", "age": 30},
    {"name": "Anne Pun", "age": 27}
]


def get_friend_name(friend):
    return friend["name"]


print(search(friends, "Rolf Smith", get_friend_name))   # this will print the dictionary with the name "Rolf Smith"

print(search(friends, "Bob Smith", get_friend_name))    # this will raise an exception, since there is no dictionary with the name "Bob Smith"

print(search(friends, "Rob Smith", itemgetter("name")))  # we can use the itemgetter function from the operator module to get the name of the dictionary