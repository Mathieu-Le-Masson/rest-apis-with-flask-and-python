a = []      # a is a reference to an object in memory, in this case a list object
b = a    # b is a reference to the same object in memory, in this case a list object

a.append(35)    # a is mutable, so we can change it, so b will also change, because they are pointing to the same object,
                # and the value of that object is changed, so the old value is lost

# In python, everything is an object, so is mutable, except for the immutable objects, like strings, tuples, integers, floats, booleans, etc.

print(id(a))
print(id(b))  # the id of a and b is the same, because they are pointing to the same object in memory

c = ()
d = c + (35,)   # c is immutable, so we can't change it, we can only create a new object

print(id(c))
print(id(d))  # the id of c and d is different, because they are pointing to different objects in memory

a = 326
b = 326       # the id of a and b is the same, because they are pointing to the same object in memory

print(id(a))
print(id(b))

a = 257      # changing the value of a, will create a new object in memory, b will still point to the old object

x = "hello"
y = x

x += "world"   # x is a string so is immutable, so we can't change it, we can only create a new object,
                  # so x will now point to a new object in memory, and y will still point to the old object,
                  # since y is just a reference to the object that x is pointing to, and this object value is not changed.
