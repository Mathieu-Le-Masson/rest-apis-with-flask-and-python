# day_of_week = input("What day of the week is it today? ").capitalize()  # capitalize() capitalizes the first letter
# day_of_week = input("What day of the week is it today? ").lower()  # lower() converts all letters to lowercase
# day_of_week = input("What day of the week is it today? ").upper()  # upper() converts all letters to uppercase
day_of_week = "Monday"

if day_of_week == "Monday":
    print("Have a great start to your week!")
elif day_of_week == "Friday":
    print("It's ok to finish a bit early!")  # elif is short for else if
else:
    print("It's not Monday nor Friday.")  # else is optional and can be used only once at the end of the if statement

print("This will execute no matter what because it is not indented.")

movies_watched = {"The Matrix", "Green Book", "Her"}
user_movie = "The Matrix"
char = "atrix"
print(user_movie in movies_watched)  # returns True because user_movie is in movies_watched
print(user_movie not in movies_watched)  # returns False because user_movie is in movies_watched
print(char in movies_watched)  # returns False because char is not in movies_watched
print(char in user_movie)  # returns True because char is a substring of user_movie

if user_movie in movies_watched:
    print(f"I've watched {user_movie} too!")  # this will execute because user_movie is in movies_watched
else:
    print(f"I haven't watched {user_movie} yet.")  # this will not execute because user_movie is in movies_watched

number = 7
# user_input = input("Enter 'y' if you would like to play: ").lower()  # lower() converts all letters to lowercase
user_input = "y"
if user_input == "y":
    user_number = int(input("Guess our number: "))
    if user_number == number:
        print("You guessed correctly!")
    elif abs(number - user_number) == 1:  # abs() returns the absolute value of a number,
                                          # we could have written this as elif(number - user_number) in (1, -1):
        print("You were off by one.")
    else:
        print("Sorry, it's wrong!")

number = 7

# user_input = input("Would you like to play? (Y/n) ").lower()  # By convention, we use Y/n to indicate that Y is the default
user_input = "y"
while user_input != "n":
    user_number = int(input("Guess our number: "))
    if user_number == number:
        print("You guessed correctly!")
    elif abs(number - user_number) == 1:  # abs() returns the absolute value of a number,
                                            # we could have written this as elif(number - user_number) in (1, -1):
        print("You were off by one.")
    else:
        print("Sorry, it's wrong!")
    # user_input = input("Would you like to play? (Y/n) ").lower()  # By convention, we use Y/n to indicate that Y is the default
    user_input = "n"

while True:  # this is an infinite loop, it will run forever unless we break out of it, we can break out of it with the break keyword
                # we can also break out of it with the return keyword, but that will exit the entire program, or the function if we are in a function
                # this is better than using a variable to control the loop because we don't need to initialize the variable,
                # and we don't repeat ourselves by checking the variable at the end of the loop
    # user_input = input("Would you like to play? (Y/n) ").lower()  # By convention, we use Y/n to indicate that Y is the default
    user_input = "n"
    if user_input == "n":
        break
    user_number = int(input("Guess our number: "))
    if user_number == number:
        print("You guessed correctly!")
    elif abs(number - user_number) == 1:  # abs() returns the absolute value of a number,
                                            # we could have written this as elif(number - user_number) in (1, -1):
        print("You were off by one.")
    else:
        print("Sorry, it's wrong!")

friends = ["Rolf", "Jen", "Bob", "Anne"]
for friend in friends:
    print(f"{friend} is my friend.")  # this will execute 4 times, once for each friend in friends

for friend in range(4):  # this will execute 4 times, once for each friend in range(4)
    print(f"{friend} is my friend.")

grades = [35, 67, 98, 100, 100]
total = 0
amount = len(grades)
for grade in grades:
    total += grade  # this is the same as total = total + grade

print(total / amount) # this will print the average of the grades

# We can also use the sum() function to get the sum of all the elements in a list
# Similar operations exist for other data types, such as min() and max() for numbers, and len() for all data types
total = sum(grades)  # sum() returns the sum of all the elements in a list
print(total / amount)  # this will print the average of the grades

for grade in grades:
    if grade % 2 == 0:  # % is the modulo operator, it returns the remainder of the division of the two operands
        print(f"{grade} is even.")

