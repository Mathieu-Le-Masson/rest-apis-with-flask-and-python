import functools

user = {"username": "jose", "access_level": "guest"}


def make_secure(func):
    @functools.wraps(func)  # this decorator will make the secure_function function have the same name as the function it is wrapping
    def secure_function():  # this is the wrapper function, which will be returned by the make_secure function
        if user["access_level"] == "admin":
            return func()
        else:
            return f"No admin permissions for {user['username']}."

    return secure_function


@make_secure  # this is the same as: get_admin_password = make_secure(get_admin_password), it is called decorating a function
def get_admin_password():
    return "1234"  # this is the admin password


print(get_admin_password())  # returns None, because the user's access level is not admin

print(get_admin_password.__name__)  # returns secure_function, because the get_admin_password function is now the secure_function function