class ClassTest:
    def instance_method(self):
        print(f"Called instance_method of {self}")

    @classmethod
    def class_method(cls):
        print(f"Called class_method of {cls}")

    @staticmethod
    def static_method():
        print("Called static_method.")


# test = ClassTest()          # test is an instance of ClassTest
# test.instance_method()      # instance_method is an instance method, so it can only be called on an instance of ClassTest.
# ClassTest.instance_method(test) # this is the same as test.instance_method(), but it is not recommended to call instance methods like this

ClassTest.class_method()  # class_method is a class method, so it can be called on the class itself
# class methods are used as factories, they are used to create instances of the class

ClassTest.static_method()  # static_method is a static method, so it can be called on the class itself


# static methods are used to place functions inside a class, because they are related to the class,
# but they don't use the class or the instance of the class


class Book:
    TYPES = ("hardcover",
             "paperback")  # TYPES is a class attribute, it is a constant, it is shared by all instances of the class

    def __init__(self, name, book_type, weight):  # __init__ is a constructor, also an instance method
        self.name = name
        self.book_type = book_type
        self.weight = weight

    def __repr__(self) -> str:  # __repr__ is a representation of the object, it is used when we print the object
        return f"<Book {self.name}, {self.book_type}, weighing {self.weight}g>"

    @classmethod
    def hardcover(cls, name, page_weight):
        return cls(name, cls.TYPES[0], page_weight + 100)

    @classmethod
    def paperback(cls, name, page_weight):
        return cls(name, cls.TYPES[1], page_weight)


book = Book.hardcover("Harry Potter", 1500)
light = Book.paperback("Python 101", 600)

print(book)
print(light)


class Store:
    def __init__(self, name):
        self.name = name
        self.items = []

    def add_item(self, name, price):
        self.items.append({
            'name': name,
            'price': price
        })

    def stock_price(self):
        total = 0
        for item in self.items:
            total += item['price']
        return total
        # It can also be written as:
        return sum([item['price'] for item in self.items])  # list comprehension, it is faster than the for loop and it is more readable

    @classmethod
    def franchise(cls, store):
        return cls(f"{store.name} - franchise")  # cls is the Store class, no need to assign a value to cls.items because it is a class attribute

    @staticmethod
    def store_details(store):
        # It should be in the format 'NAME, total stock price: TOTAL'
        return f"{store.name}, total stock price: {int(store.stock_price())}"
        # It can also be written as:
        return "{}, total stock price: {}".format(store.name, int(store.stock_price())) # .format() is a string method,
