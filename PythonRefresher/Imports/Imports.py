from My_module import divide    # We can import modules from other files in the same directory
import sys                   # sys is a module that contains system-specific parameters and functions


print(__name__)             # this is the __main__ module, the print(__name__) in My_module.divide returns My_module, because it is imported
print(divide(10, 2))        # this is the __main__ module, the print(__name__) in My_module.divide returns My_module, because it is imported

print(sys.path)             # sys.path is a list of directories where Python will look for modules