users = [
    (0, "Bob", "password"),
    (1, "Rolf", "bob123"),
    (2, "Jose", "long4password"),
    (3, "username", "1234"),
]

username_mapping = {user[1]: user for user in users}    # dictionary comprehension
# username_mapping = {user[1]: user for user in users} is the same as:
for user in users:
    if user[1] == "Bob":
        username_mapping["Bob"] = user

print(username_mapping["Bob"])  # returns (0, "Bob", "password")

username_input = input("Enter your username: ")
password_input = input("Enter your password: ")

_, username, password = username_mapping[username_input]  # we use _ to indicate that we don't care about the first value in the tuple

if password_input == password:
    print("Your details are correct!")
else:
    print("Your details are incorrect.")
