import functools

user = {"username": "jose", "access_level": "guest"}


def make_secure(func):
    @functools.wraps(func)  # this decorator will make the secure_function function have the same name as the function it is wrapping
    def secure_function(*args, **kwargs):  # we are adding *args and **kwargs to the wrapper function, so that it can accept any number of arguments
        if user["access_level"] == "admin":
            return func(*args, **kwargs)
        else:
            return f"No admin permissions for {user['username']}."

    return secure_function


@make_secure  # this is the same as: get_admin_password = make_secure(get_admin_password), it is called decorating a function
def get_password(panel):
    if panel == "admin":
        return "1234"  # this is the admin password
    elif panel == "billing":
        return "super_secure_password"


print(get_password("billing"))  # returns None, because the user's access level is not admin
print(get_password.__name__)  # returns get_password, thanks to the functools.wraps decorator
