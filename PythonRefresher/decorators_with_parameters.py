import functools

user = {"username": "jose", "access_level": "guest"}   # access levels are: admin, user, guest; it could be easily changed to a number, like 1, 2, 3


def make_secure(access_level):  # we are adding the access_level parameter to the make_secure function
    def decorator(func):  # this is the decorator function, which will be returned by the make_secure function
        @functools.wraps(func)  # this decorator will make the secure_function function have the same name as the function it is wrapping
        def secure_function(*args, **kwargs):  # we are adding *args and **kwargs to the wrapper function, so that it can accept any number of arguments
            if user["access_level"] == access_level:  # we are checking if the user's access level is the same as the access level parameter
                return func(*args, **kwargs)
            else:
                return f"No {access_level} permissions for {user['username']}."

        return secure_function

    return decorator


@make_secure("admin")  # this is the same as: get_admin_password = make_secure(get_admin_password), it is called decorating a function
def get_admin_password():
        return "admin: 1234"     # this is the admin password


@make_secure("user")  # this is the same as: get_dashboard_password = make_secure(get_dashboard_password), it is called decorating a function
def get_dashboard_password():
    return "user:user_password"  # this is the dashboard password


print(get_admin_password())
print(get_dashboard_password())
