def multiply(*args):               # *args is a tuple, it is a tuple of all the arguments that are passed in
    print(args)                    # (1, 3, 5)
    total = 1
    for arg in args:             # args is a tuple, so we can iterate over it
        total = total * arg
    return total


def add(x, y):
    return x + y


def apply(*args, operator):  # *args is a tuple composed of all the arguments that are passed in,
                            # operator is a keyword argument,
                            # we need to specify the operator when we call the function by using operator="*"
    if operator == "*":
        return multiply(*args)  # *args is the same as 1, 3, 5 (the * unpacks the tuple)
    elif operator == "+":
        return sum(args)  # *args is the same as 1, 3, 5 (the * unpacks the tuple)
    else:
        return "No valid operator provided to apply()."


print(multiply(1, 3, 5))
print(multiply(-1))

nums = [3, 5]
print(add(*nums))  # *nums is the same as 3, 5 (the * unpacks the list)

nums2 = {"x": 15, "y": 25}
print(add(x=nums2["x"], y=nums2["y"]))  # this is the same as add(x=15, y=25)
print(add(**nums2))  # **nums2 is the same as x=15, y=25 (the ** unpacks the dictionary), the keys must match the parameter names

print(apply(1, 3, 5, operator="*"))  # returns 15
print(apply(1, 3, 5, operator="+"))  # returns 9
print(apply(1, 3, 5, operator="-"))  # returns "No valid operator provided to apply()."

# So when we do (apply(1, 3, 5, operator="*")), the function will be called like this:
# apply(*(1, 3, 5), operator="*")
# the arguments will first be packed into a tuple, and then the tuple will be unpacked into the function
# we do this because we don't know how many arguments will be passed in, so we pack them into a tuple